#!/bin/sh

set -e
set -x

DUMP_FILE_NAME="${FILE_NAME}-$(date +"%Y-%m-%d_%Hh%M").dump.sql"
echo "Creating dump"

mysqldump -A -u ${MYSQL_USER} -p${MYSQL_PASSWORD} -h ${MYSQL_HOST} -P ${MYSQL_PORT}  > backup.sql

if [[ $? -ne 0 ]]; then
  echo "Back up not created, check db connection settings"
  exit 1
fi

swift --os-project-name "${OS_PROJECT_NAME}" -V=3 --os-auth-url "${OS_AUTH_URL}" --os-storage-url "${OS_ENDPOINT}" upload "${OS_BUCKET_NAME}" backup.sql --object-name "${DUMP_FILE_NAME}"

rm backup.sql
